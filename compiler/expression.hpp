#pragma once
#include <vector>
#include <string>
#include <list>
#include <iostream>
#include "semantics.hpp"

class Call;
class Atom;

using namespace std;

extern SymbolTable st;

class Expression
{
protected:
	T_Expr_Type expr_type;
	TonyType *type;
	Expression *expr1, *expr2;
	string op;
	Atom *atom;
	Call *call;
	string const_c;
	int const_int;

public:
	void sem();
	Value* codegen();
	TonyType *getType();
	T_Expr_Type getExprType();
	Atom *getAtom();
	friend ostream &operator<<(ostream &os, const Expression *expr)
	{
		switch (expr->expr_type)
		{
		case T_EXPR_BINARY:
			os << "(" << expr->expr1 << " " << expr->op << " " << expr->expr2 << ")";
			break;
		case T_EXPR_UNARY:
			if (expr->op.compare("()") != 0)
				os << "(" << expr->op << expr->expr1 << ")";
			else
				os << "(" << expr->expr1 << ")";
			break;
		case T_EXPR_TRUE:
			os << "(TRUE)";
			break;
		case T_EXPR_FALSE:
			os << "(FALSE)";
			break;
		case T_EXPR_NIL:
			os << "(NIL)";
			break;
		case T_EXPR_CALL:
			os << expr->call;
			break;
		case T_EXPR_CONST_INT:
			os << expr->const_int;
			break;
		case T_EXPR_CONST_CHAR:
			os << "'" << expr->const_c << "'";
			break;
		case T_EXPR_ATOM:
			os << expr->atom;
			break;
		}
		return os;
	}
	~Expression();

	Expression(Expression *_expr, string _op);

	Expression(Expression *_expr1, Expression *_expr2, string _op);

	Expression(Call *_call);

	Expression(Atom *_atom);

	Expression(T_Expr_Type _expr_type);

	Expression(Expression *_expr, TonyType *_type);

	Expression(string c);

	Expression(int i);
};
