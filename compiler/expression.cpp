#include "expression.hpp"
#include <string.h>

using namespace std;

void Expression::sem()
{
	try
	{
		switch (expr_type)
		{
		case T_EXPR_BINARY:
			expr1->sem();
			expr2->sem();
			/* CHECK LOWER LEVEL TYPES */
			T_Type_Type t1, t2;
			t1 = expr1->getType()->get_Type();
			t2 = expr2->getType()->get_Type();

			if ((op.compare("+") == 0) || (op.compare("-") == 0) || (op.compare("*") == 0) || (op.compare("/") == 0))
				if ((t1 != T_TYPE_INT) || (t2 != T_TYPE_INT))
				{
					cout << "Error: operator argument is not int: " << this << endl;
					exit(1);
				}
				else
					type = expr1->getType();

			if ((op.compare("and") == 0) || (op.compare("or") == 0))
				if ((t1 != T_TYPE_BOOL) || (t2 != T_TYPE_BOOL))
				{
					cout << "Error: operator argument is not bool: " << this << endl;
					exit(1);
				}
				else
					type = expr1->getType();

			if (op.compare("#") == 0)
				if (expr2->getType()->get_Type() != T_TYPE_LIST)
				{
					cout << "Error: operator argument is not list (2nd arg): " << this << endl;
					exit(1);
				}
				else if (expr1->getType()->equals(expr2->getType()->getType()) != 0)
				{
					cout << "Error: non matching types in # operator: " << this << endl;
					exit(1);
				}
				else
					type = expr2->getType();

			if ((op.compare("=") == 0) || (op.compare("<>") == 0) || (op.compare("<") == 0) || (op.compare(">") == 0) || (op.compare("<=") == 0) || (op.compare(">=") == 0))
			{
				if (t1 != t2)
				{
					cout << "Error: non matching types in comparison operator: " << this << endl;
					exit(1);
				}
				else if (t1 != T_TYPE_BOOL && t1 != T_TYPE_INT && t1 != T_TYPE_CHAR)
				{
					cout << "Error: non primitive type in comparison operator: " << this << endl;
					exit(1);
				}
				else
					type = new TonyType(T_TYPE_BOOL);
			}

			break;
		case T_EXPR_UNARY:
			expr1->sem();
			/* CHECK LOWER LEVEL TYPES */
			T_Type_Type t;
			t = expr1->getType()->get_Type();

			if (op.compare("()") == 0)
				type = expr1->getType();

			if ((op.compare("+") == 0) || (op.compare("-") == 0))
				if (t != T_TYPE_INT)
				{
					cout << "Error: operator argument is not int: " << this << endl;
					exit(1);
				}
				else
					type = expr1->getType();

			if (op.compare("not") == 0)
				if (t != T_TYPE_BOOL)
				{
					cout << "Error: operator argument is not bool: " << this << endl;
				}
				else
					type = expr1->getType();

			if ((op.compare("nil?") == 0) || (op.compare("head") == 0) || (op.compare("tail") == 0))
				if (t != T_TYPE_LIST)
				{
					cout << "Error: operator argument is not a list: " << this << endl;
					exit(1);
				}
				else if (op.compare("nil?") == 0)
					type = new TonyType(T_TYPE_BOOL);
				else if (op.compare("head") == 0)
					type = expr1->getType()->getType();
				else if (op.compare("tail") == 0)
					type = expr1->getType();

			if (op.compare("new") == 0)
				if (expr1->getType()->get_Type() != T_TYPE_INT)
				{
					cout << "Error: operator argument is not int: " << this << endl;
					exit(1);
				}
				else
				{
					TonyType *tmp = type;
					type = new TonyType(tmp, T_TYPE_ARRAY);
				}

			break;
		case T_EXPR_TRUE:
			type = new TonyType(T_TYPE_BOOL);
			break;
		case T_EXPR_FALSE:
			type = new TonyType(T_TYPE_BOOL);
			break;
		case T_EXPR_NIL:
			type = new TonyType(T_TYPE_LIST);
			break;
		case T_EXPR_CALL:
			call->sem();
			type = call->getType();
			break;
		case T_EXPR_CONST_INT:
			type = new TonyType(T_TYPE_INT);
			break;
		case T_EXPR_CONST_CHAR:
			type = new TonyType(T_TYPE_CHAR);
			break;
		case T_EXPR_ATOM:
			atom->sem();
			type = atom->getType();
			break;
		}
	}
	catch (exception e)
	{
		cout << "Caught!";
		exit(0);
	}
}

Value* Expression::codegen() {
	
}

Atom *Expression::getAtom()
{
	return atom;
}

TonyType *Expression::getType()
{
	if (type != nullptr)
		return type;
	else
		cout << "Expression Type is null" << endl;
}

T_Expr_Type Expression::getExprType()
{
	return expr_type;
}

Expression::~Expression()
{
}

Expression::Expression(Expression *_expr, string _op)
{
	expr1 = _expr;
	op = _op;
	expr_type = T_EXPR_UNARY;
}

Expression::Expression(Expression *_expr1, Expression *_expr2, string _op)
{
	expr1 = _expr1;
	expr2 = _expr2;
	op = _op;
	expr_type = T_EXPR_BINARY;
}

Expression::Expression(Call *_call)
{
	call = _call;
	expr_type = T_EXPR_CALL;
}

Expression::Expression(Atom *_atom)
{
	atom = _atom;
	expr_type = T_EXPR_ATOM;
}

Expression::Expression(T_Expr_Type _expr_type)
{
	expr_type = _expr_type;
}

Expression::Expression(Expression *_expr, TonyType *_type)
{
	expr1 = _expr;
	type = _type;
	expr_type = T_EXPR_UNARY;
	op = "new";
}

Expression::Expression(string c)
{
	const_c = c.substr(1, c.length() - 2);
	expr_type = T_EXPR_CONST_CHAR;
}

Expression::Expression(int i)
{
	const_int = i;
	expr_type = T_EXPR_CONST_INT;
}
