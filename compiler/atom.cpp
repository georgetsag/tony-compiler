#include "atom.hpp"

using namespace std;

void Atom::sem()
{
	switch (atom_type)
	{
	case T_ATOM_ID:
		SymbolEntry *se;
		se = st.lookup(id->getName());
		type = se->type;
		break;
	case T_ATOM_CONST_STRING:
		TonyType *tmp;
		tmp = new TonyType(T_TYPE_CHAR);
		type = new TonyType(tmp, T_TYPE_ARRAY);
		break;
	case T_ATOM_ARRAY:
		expr->sem();
		atom->sem();
		if (expr->getType()->get_Type() != T_TYPE_INT)
			cout << "Error: Non integer value inside index brackets [] for " << this << endl;
		if (atom->getType()->get_Type() == T_TYPE_ARRAY)
		{
			type = atom->getType()->getType();
		}
		else
		{
			cout << "Error: Something went wrong while dereferencing " << this << endl;
			exit(1);
		}

		break;
	case T_ATOM_CALL:
		call->sem();
		type = call->getType();
		break;
	}
}

Value* Atom::codegen() {
	
}

TonyType *Atom::getType()
{
	if (type != nullptr)
		return type;
	else
		error("Atom getType is nullptr");
}

T_Atom_Type Atom::getAtomType()
{
	return atom_type;
}

Atom::Atom(Id *_id)
{
	id = _id;
	atom_type = T_ATOM_ID;
}

Atom::Atom(string _str)
{
	str = _str.substr(1, str.length() - 2);
	atom_type = T_ATOM_CONST_STRING;
}

Atom::Atom(Atom *_atom, Expression *_expr)
{
	atom_type = T_ATOM_ARRAY;
	atom = _atom;
	expr = _expr;
}
Atom::Atom(Call *_call)
{
	atom_type = T_ATOM_CALL;
	call = _call;
}
Atom::~Atom()
{
}
