/*DEFINITIONS*/

%option noyywrap

%{

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <string.h>
#include "lexer.hpp"
#include "semantics.hpp"
#include "parser.hpp"

int linecount = 1;
int nested_count = 0;
%}


%x IN_COMMENT

L ([a-z]|[A-Z])
D [0-9]
W [\ \t\r\n]
H ([0-9]|[a-f]|[A-F])
O [,\./;\[\]\{\}=!\+\-_<>\?:\\|]
SEP [\(\)\[\],;:]|":="
SYM [\+\-\*/#=<>]|"<>"|"<="|">="

/*DEFINITIONS END*/

/*PATTERN RULES*/

%%

<INITIAL>{
"<*"		{nested_count++;BEGIN(IN_COMMENT);}
}
<IN_COMMENT>{
"<*"		{nested_count++;}
"*>"		{nested_count--;if(nested_count==0)BEGIN(INITIAL);}
.		{}

{W}+		{countlines();}
}

"and"		{return  T_and;}
"bool"		{return  T_bool;}
"char"		{return  T_char;}
"decl"		{return  T_decl;}
"def"		{return  T_def;}
"else"		{return  T_else;}
"elsif"		{return  T_elif;}
"end"		{return  T_end;}
"exit"		{return  T_exit;}
"false"		{return  T_false;}
"for"		{return  T_for;}
"head"		{return  T_head;}
"if"		{return  T_if;}
"int"		{return  T_int;}
"list"		{return  T_list;}
"mod"		{return  T_mod;}
"new"		{return  T_new;}
"nil"		{return  T_nil;}
"nil?"		{return  T_nil_;}
"not"		{return  T_not;}
"or"		{return  T_or;}
"ref"		{return  T_ref;}
"return"	{return  T_return;}
"skip"		{return  T_skip;}
"tail"		{return  T_tail;}
"true"		{return  T_true;}

({L})+({L}|{D}|[_]|[?])*				{yylval.sval = strdup(yytext); return T_id;}
{D}+							{yylval.ival = atoi(yytext); return T_const_int;}
%.*							{}
'({L}|{D}|\\|(\\[ntr0\\'"])|(\\x({H}{2})))'		{yylval.sval = strdup(yytext); return T_const_char;}
\"({L}|{D}|\\|\ |{O}|(\\[ntr0\\'"])|(\\x({H}{2})))*\"	{yylval.sval = strdup(yytext); return T_const_string;}
{SEP}							{
							if(yyleng==1) return yytext[0];
							else if(yyleng==2){
								if(strcmp(yytext, ":=")==0)
								return T_assign;
							}
							
							}
{SYM}							{
							if(yyleng==1) return yytext[0];
							else if(yyleng==2){
								if(strcmp(yytext, "<>")==0)
								return T_not_equal;
								if(strcmp(yytext, "<=")==0)
								return T_less_equal;
								if(strcmp(yytext, ">=")==0)
								return T_greater_equal;
							}
							}
							
							
\n							{countlines();}
{W}+							{countlines();}
.							{printf("error on line %d\n", linecount); exit(1);}
<<EOF>>							{return T_eof;}
%%

/*PATTERN RULES END*/

/*USER CODE*/

void countlines(){
	for(int i = 0; i < yyleng; i++)
		if(yytext[i]=='\n')
			linecount++;
}

/*USER CODE END*/
