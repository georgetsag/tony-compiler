#pragma once
#include <iostream>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/LegacyPassManager.h>
#include <llvm/IR/Value.h>
#include <llvm/IR/Verifier.h>
#include <llvm/Transforms/InstCombine/InstCombine.h>
#include <llvm/Transforms/Scalar.h>
#include <llvm/Transforms/Scalar/GVN.h>
#include <llvm/Transforms/Utils.h>

using namespace llvm;

class TonyBuilder
{
public:
  static void build(bool optimize = true)
  {
    // Initialize
    TheModule = make_unique<Module>("tony_program", TheContext);
    TheFPM = make_unique<legacy::FunctionPassManager>(TheModule.get());
    if (optimize)
    {
      TheFPM->add(createPromoteMemoryToRegisterPass());
      TheFPM->add(createInstructionCombiningPass());
      TheFPM->add(createReassociatePass());
      TheFPM->add(createGVNPass());
      TheFPM->add(createCFGSimplificationPass());
    }
    TheFPM->doInitialization();

    // Initialize types
    i8 = IntegerType::get(TheContext, 8);
    i32 = IntegerType::get(TheContext, 32);
    i64 = IntegerType::get(TheContext, 64);

    StructType *NodeType = StructType::create(TheContext, "nodetype");
    NodeType->setBody({i64, i64});
    TheNodeTypePtr = PointerType::get(NodeType, 0);

    // Initialize global variables



    // ArrayType *vars_type = ArrayType::get(i64, 26);
    // TheVars = new GlobalVariable(
    //     *TheModule, vars_type, false, GlobalValue::PrivateLinkage,
    //     ConstantAggregateZero::get(vars_type), "vars");
    // TheVars->setAlignment(16);
    ArrayType *nl_type = ArrayType::get(i8, 2);
    TheNL = new GlobalVariable(
        *TheModule, nl_type, true, GlobalValue::PrivateLinkage,
        ConstantArray::get(nl_type, {c8('\n'), c8('\0')}), "nl");
    TheNL->setAlignment(1);

    // Initialize library functions
    FunctionType *writeInteger_type = FunctionType::get(Type::getVoidTy(TheContext), {i64}, false);
    TheWriteInteger = Function::Create(writeInteger_type, Function::ExternalLinkage, "writeInteger", TheModule.get());

    FunctionType *writeString_type = FunctionType::get(Type::getVoidTy(TheContext), {PointerType::get(i8, 0)}, false);
    TheWriteString = Function::Create(writeString_type, Function::ExternalLinkage, "writeString", TheModule.get());

    FunctionType *writeCharacter_type = FunctionType::get(Type::getVoidTy(TheContext), {i8}, false);
    TheWriteCharacter = Function::Create(writeCharacter_type, Function::ExternalLinkage, "writeCharacter", TheModule.get());

    FunctionType *writeBoolean_type = FunctionType::get(Type::getVoidTy(TheContext), {i8}, false);
    TheWriteBoolean = Function::Create(writeBoolean_type, Function::ExternalLinkage, "writeBoolean", TheModule.get());

    // Initialize library functions
    FunctionType *malloc_type =
        FunctionType::get(PointerType::get(i8, 0), {i64}, false);
    TheMalloc =
        Function::Create(malloc_type, Function::ExternalLinkage,
                         "GC_malloc", TheModule.get());
    FunctionType *init_type =
        FunctionType::get(Type::getVoidTy(TheContext), {}, false);
    TheInit =
        Function::Create(init_type, Function::ExternalLinkage,
                         "GC_init", TheModule.get());

    // Define and start the main function.
    FunctionType *main_type = FunctionType::get(i32, {}, false);
    Function *main =
        Function::Create(main_type, Function::ExternalLinkage,
                         "main", TheModule.get());
    BasicBlock *BB = BasicBlock::Create(TheContext, "entry", main);
    Builder.SetInsertPoint(BB);
    Builder.CreateCall(TheInit, {});

    // Emit the program code.

    // TODO
    //compile();

    Builder.CreateRet(c32(0));
    // Verify the IR.
    bool bad = verifyModule(*TheModule, &errs());
    if (bad)
    {
      std::cerr << "The IR is bad!" << std::endl;
      TheModule->print(errs(), nullptr);
      std::exit(1);
    }
    // Optimize!
    TheFPM->run(*main);
    // Print out the IR.
    TheModule->print(outs(), nullptr);
  }
  static LLVMContext TheContext;
  static IRBuilder<> Builder;
  static std::unique_ptr<Module> TheModule;
  static std::unique_ptr<legacy::FunctionPassManager> TheFPM;
  static std::map<std::string, Value *> NamedValues;

  static GlobalVariable *TheVars;
  static GlobalVariable *TheNL;
  static Function *TheWriteInteger;
  static Function *TheWriteBoolean;
  static Function *TheWriteCharacter;
  static Function *TheWriteString;
  static Function *TheInit;
  static Function *TheMalloc;

  static Type *i8;
  static Type *i32;
  static Type *i64;
  static Type *TheNodeTypePtr;

  static ConstantInt *c8(char c)
  {
    return ConstantInt::get(TheContext, APInt(8, c, true));
  }
  static ConstantInt *c32(int n)
  {
    return ConstantInt::get(TheContext, APInt(32, n, true));
  }
  static ConstantInt *c64(int n)
  {
    return ConstantInt::get(TheContext, APInt(64, n, true));
  }
};