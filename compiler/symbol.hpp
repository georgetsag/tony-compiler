#pragma once
#include <string>
#include <cstdlib>
#include <vector>
#include "utilities.hpp"
#include "semantics.hpp"
#include "formal.hpp"
#include <map>

class TonyType;
class Formal;

extern int formal_equals(Formal *formal, Formal *_formal);

struct SymbolEntry
{
  TonyType *type;
  T_Symbol_Type symbol_type;
  int offset;
  _List<Formal> *func_args;
  int decl_flag;
  SymbolEntry() {}
  SymbolEntry(TonyType *t, int ofs) : type(t), offset(ofs) { symbol_type = T_SYMBOL_VAR; }
  SymbolEntry(TonyType *t, _List<Formal> *arg_list, int ofs) : type(t), func_args(arg_list), offset(ofs)
  {
    symbol_type = T_SYMBOL_FUNC;
    decl_flag = 0;
  }
  SymbolEntry(TonyType *t, _List<Formal> *arg_list, int ofs, int _decl_flag) : type(t), func_args(arg_list), offset(ofs)
  {
    decl_flag = 1;
    symbol_type = T_SYMBOL_FUNC;
  }
};

class Scope
{
public:
  Scope() : locals(), offset(-1), size(0) {}

  Scope(int ofs) : locals(), offset(ofs), size(0) {}

  int getOffset() const { return offset; }

  int getSize() const { return size; }

  SymbolEntry *lookup(string c)
  {
    if (locals.find(c) == locals.end())
      return nullptr;
    return &(locals[c]);
  }

  void insert(string c, TonyType *t)
  {
    try
    {
      if (locals.find(c) != locals.end())
      {
        std::cerr << "Duplicate variable " << c << std::endl;
        exit(1);
      }
      locals[c] = SymbolEntry(t, offset++);
      ++size;
    }
    catch (std::exception &e)
    {
      cout << "Failed to insert SE" << endl;
      exit(1);
    }
  }

  void insert(string c, _List<Formal> *_formal_list, TonyType *t)
  {
    try
    {
      if (locals.find(c) != locals.end())
      {
        if (locals[c].decl_flag == 0)
        {
          std::cerr << "Error: Duplicate id " << c << std::endl;
          exit(1);
        }
        else
        {
          if (t->equals(locals[c].type) != 0)
          {
            cout << "Error: definition of function '" << c << "' has different return type: " << t << ", expected: " << locals[c].type << endl;
            exit(1);
          }
          _List<Formal> *formal_list;
          formal_list = locals[c].func_args;
          if ((formal_list == nullptr && _formal_list != nullptr) ||
              (formal_list != nullptr && _formal_list == nullptr))
          {
            cout << "Error: formals not matching between declaration and definition of function " << c << endl;
            exit(1);
          }
          if (formal_list != nullptr && _formal_list != nullptr)
          {
            if (_formal_list->length() != formal_list->length())
            {
              cout << "Error: formals not matching between declaration and definition of function " << c << endl;
              exit(1);
            }
            for (int i = 0; i < formal_list->length(); i++)
            {
              if (formal_equals(formal_list->at(i), _formal_list->at(i)) != 0)
              {
                cout << "Error: formals not matching between declaration and definition of function " << c << endl;
                exit(1);
              }
            }
          }
          return;
        }
      }
      locals[c] = SymbolEntry(t, _formal_list, offset++);
      ++size;
    }
    catch (std::exception &e)
    {
      cout << "Error: Failed to insert SE" << endl;
      exit(1);
    }
  }

  void insert(string c, _List<Formal> *_formal_list, TonyType *t, int decl_flag)
  {
    try
    {
      if (locals.find(c) != locals.end())
      {
        std::cerr << "Error: Duplicate id " << c << std::endl;
        exit(1);
      }
      locals[c] = SymbolEntry(t, _formal_list, offset++, decl_flag);
      ++size;
    }
    catch (std::exception &e)
    {
      cout << "Error: Failed to insert SE" << endl;
      exit(1);
    }
  }

private:
  std::map<string, SymbolEntry> locals;
  int offset;
  int size;
};

class SymbolTable
{
public:
  void openScope()
  {
    int ofs = scopes.empty() ? 0 : scopes.back().getOffset();
    scopes.push_back(Scope(ofs));
  }

  void closeScope() { scopes.pop_back(); };

  SymbolEntry *lookup(string c)
  {
    for (auto i = scopes.rbegin(); i != scopes.rend(); ++i)
    {
      SymbolEntry *e = i->lookup(c);
      if (e != nullptr)
        return e;
    }
    std::cerr << "Unknown id " << c << std::endl;
    exit(1);
  }

  int getSizeOfCurrentScope() const { return scopes.back().getSize(); }

  void insert(string c, TonyType *t) { scopes.back().insert(c, t); }

  void insert(string c, _List<Formal> *_formal_list, TonyType *t) { scopes.back().insert(c, _formal_list, t); }

  void insert(string c, _List<Formal> *_formal_list, TonyType *t, int decl_flag) { scopes.back().insert(c, _formal_list, t, decl_flag); }

private:
  std::vector<Scope> scopes;
};
