#include "formal.hpp"

using namespace std;

void Formal::sem()
{
	for (int i = 0; i < id_list->length(); i++)
		st.insert(id_list->at(i)->getName(), type);
}

Value *Formal::codegen()
{
}

TonyType *Formal::getType()
{
	return type;
}

_List<Id> *Formal::getIdList()
{
	return id_list;
}

int Formal::getRefType()
{
	return ref_val;
}

int Formal::equals(Formal *_formal)
{
	if (this->getType()->equals(_formal->getType()) != 0)
	{
		cout << "Error: formals not compatible:" << endl
				 << "\t" << this << endl
				 << "\t" << _formal << endl;
		return 1;
	}
	if (id_list->length() != _formal->getIdList()->length())
	{
		cout << "Error: formals not compatible:" << endl
				 << "\t" << this << endl
				 << "\t" << _formal << endl;
		return 1;
	}
	for (int i = 0; i < id_list->length(); i++)
	{
		if (id_list->at(i)->getName().compare(_formal->getIdList()->at(i)->getName()) != 0)
		{
			cout << "Error: formals not compatible:" << endl
					 << "\t" << this << endl
					 << "\t" << _formal << endl;
			cout << "\tFunction argument identifiers must be the same: " << id_list->at(i)->getName() << " " << _formal->getIdList()->at(i)->getName() << endl;
			return 1;
		}
	}
	return 0;
}

Formal::Formal(int _ref_val, TonyType *_type, _List<Id> *_id_list)
{
	ref_val = _ref_val;
	type = _type;
	id_list = _id_list;
}
