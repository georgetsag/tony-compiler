#pragma once
#include <vector>
#include <string>
#include <list>
#include <iostream>
#include <stack>
#include "llvm_utils.hpp"
#include "utilities.hpp"
#include "error.h"
#include "symbol.hpp"
#include "atom.hpp"
#include "expression.hpp"
#include "else_if.hpp"
#include "formal.hpp"
#include "header.hpp"

using namespace std;

class Definition;
class Call;
class Simple;
class Header;
class Statement;
class Atom;
class Else_If;
class Formal;
extern SymbolTable st;
extern stack<TonyType *> type_stack;

ostream &operator<<(ostream &os, const Statement *stmt);
ostream &operator<<(ostream &os, const Atom *atom);

int formal_equals(Formal *formal, Formal *_formal);

class Call
{
private:
	Id *id;
	TonyType *type;
	_List<Expression> *arg_list;

public:
	Call(Id *_id, _List<Expression> *_arg_list);

	Call(Id *_id);

	void sem();
	Value* codegen();

	TonyType *getType();

	static int matchCallArguments(_List<Expression> *arg_list, _List<Formal> *formal_list);

	friend ostream &operator<<(ostream &os, const Call *call)
	{
		os << call->id->getName() << "(";
		if (call->arg_list != nullptr)
			for (int i = 0; i < call->arg_list->length(); i++)
				os << call->arg_list->at(i) << ",";
		os << ")";
		return os;
	}
};

class Simple
{
private:
	T_Simple_Type simple_type;

	Atom *atom;
	Expression *expr;
	Call *call;

public:
	void sem();
	Value* codegen();

	Simple(T_Simple_Type _type);

	Simple(Atom *_atom, Expression *_expr);

	Simple(Call *_call);

	friend ostream &operator<<(ostream &os, const Simple *simple)
	{
		switch (simple->simple_type)
		{
		case T_SIMPLE_SKIP:
			os << "skip";
			break;
		case T_SIMPLE_ASSIGN:
			os << simple->atom << ":=" << simple->expr;
			break;
		case T_SIMPLE_CALL:
			os << simple->call;
			break;
		}
		return os;
	}
};

class Statement
{
private:
	T_Statement_Type stmt_type;
	Simple *simple;
	Expression *expr;
	_List<Statement> *stmt_list_1, *stmt_list_2;
	_List<Else_If> *else_if_list;
	_List<Simple> *simple_list_1, *simple_list_2;

public:
	void sem();
	Value* codegen();

	friend ostream &operator<<(ostream &os, const Statement *stmt)
	{
		switch (stmt->stmt_type)
		{
		case T_STMT_SIMPLE:
			os << stmt->simple;
			break;
		case T_STMT_EXIT:
			os << "exit";
			break;
		case T_STMT_RETURN:
			os << "return";
			break;
		case T_STMT_IF:
			os << "If:" << stmt->expr << endl;
			for (int i = 0; i < stmt->stmt_list_1->length(); i++)
				os << stmt->stmt_list_1->at(i) << endl;
			break;
		case T_STMT_IF_ELIF:
			os << "If: " << stmt->expr << endl;
			for (int i = 0; i < stmt->stmt_list_1->length(); i++)
				os << stmt->stmt_list_1->at(i) << endl;
			for (int i = 0; i < stmt->else_if_list->length(); i++)
				os << stmt->else_if_list->at(i) << endl;
			os << "If END" << endl;
			break;
		case T_STMT_IF_ELSE:
			os << "If: " << stmt->expr << endl;
			for (int i = 0; i < stmt->stmt_list_1->length(); i++)
				os << stmt->stmt_list_1->at(i) << endl;
			os << "Else:" << endl;
			for (int i = 0; i < stmt->stmt_list_2->length(); i++)
				os << stmt->stmt_list_2->at(i) << endl;

			break;
		case T_STMT_IF_ELIF_ELSE:
			os << "If: " << stmt->expr << endl;
			for (int i = 0; i < stmt->stmt_list_1->length(); i++)
				os << stmt->stmt_list_1->at(i) << endl;
			for (int i = 0; i < stmt->else_if_list->length(); i++)
				os << stmt->else_if_list->at(i) << endl;
			os << "Else:" << endl;
			for (int i = 0; i < stmt->stmt_list_2->length(); i++)
				os << stmt->stmt_list_2->at(i) << endl;
			break;
		case T_STMT_FOR:
			os << "For: ";
			break;
		}
		return os;
	}

	Statement(Simple *_simple);

	Statement(T_Statement_Type _stmt_type);

	Statement(Expression *_expr, T_Statement_Type _stmt_type);

	Statement(Expression *_expr, _List<Statement> *_stmt_list);

	Statement(Expression *_expr, _List<Statement> *_stmt_list, _List<Else_If> *_else_if_list);
	Statement(Expression *_expr, _List<Statement> *_stmt_list_1, _List<Statement> *_stmt_list_2);

	Statement(Expression *_expr, _List<Statement> *_stmt_list_1, _List<Statement> *_stmt_list_2, _List<Else_If> *_else_if_list);

	Statement(Expression *_expr, _List<Simple> *_simple_list_1, _List<Simple> *_simple_list_2, _List<Statement> *_stmt_list);
};

class Definition
{
private:
	T_Def_Type def_type;
	TonyType *type;
	Header *header;
	_List<Definition> *def_list;
	_List<Statement> *stmt_list;
	_List<Id> *id_list;

public:
	void sem();
	void init();
	Value* codegen();

	friend ostream &operator<<(ostream &os, const Definition *def)
	{
		switch (def->def_type)
		{
		case T_FUNC_DEF:
			os << "Fun Def: " << def->header << endl;
			if (def->def_list != nullptr)
			{
				for (int i = 0; i < def->def_list->length(); i++)
					if (def->def_list->at(i)->get_def_type() != T_FUNC_DEF)
						os << def->def_list->at(i) << endl;
			}
			os << "Statements: " << endl;
			for (int i = 0; i < def->stmt_list->length(); i++)
				os << def->stmt_list->at(i) << endl;
			os << "END " << def->header << endl;
			break;
		case T_FUNC_DECL:
			os << "Fun Decl: " << def->header << endl;
			break;
		case T_VAR_DEF:
			os << def->type << ":";
			for (int i = 0; i < def->id_list->length(); i++)
				os << def->id_list->at(i) << ";";
			break;
		}
		return os;
	}

	T_Def_Type get_def_type();

	Definition(TonyType *t, _List<Id> *list);

	Definition(Header *_header);

	Definition(Header *_header, _List<Definition> *_def_list, _List<Statement> *_stmt_list);

	T_Def_Type getDefType();
};
