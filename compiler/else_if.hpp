#pragma once
#include "semantics.hpp"

class Statement;

using namespace std;

class Else_If
{
private:
	Expression *expr;
	_List<Statement> *stmt_list;

public:
	Else_If(Expression *_expr, _List<Statement> *_stmt_list);

	void sem();
	Value* codegen();
	friend ostream &operator<<(ostream &os, const Else_If *else_if)
	{
		os << "ElseIf: " << else_if->expr << endl;
		for (int i = 0; i < else_if->stmt_list->length(); i++)
			os << else_if->stmt_list->at(i) << endl;
		os << "ElseIf END" << endl;
	}
};
