#pragma once
using namespace std;
#include "semantics.hpp"

typedef enum
{
	T_SYMBOL_VAR,
	T_SYMBOL_FUNC
} T_Symbol_Type;

typedef enum
{
	T_TYPE_NONE,
	T_TYPE_INT,
	T_TYPE_BOOL,
	T_TYPE_CHAR,
	T_TYPE_ARRAY,
	T_TYPE_LIST
} T_Type_Type;

typedef enum
{
	T_FUNC_DEF,
	T_FUNC_DECL,
	T_VAR_DEF
} T_Def_Type;

typedef enum
{
	T_SIMPLE_SKIP,
	T_SIMPLE_ASSIGN,
	T_SIMPLE_CALL
} T_Simple_Type;

typedef enum
{
	T_ATOM_ID,
	T_ATOM_CONST_STRING,
	T_ATOM_ARRAY,
	T_ATOM_CALL
} T_Atom_Type;

typedef enum
{
	T_STMT_SIMPLE,
	T_STMT_EXIT,
	T_STMT_RETURN,
	T_STMT_IF,
	T_STMT_IF_ELIF,
	T_STMT_IF_ELSE,
	T_STMT_IF_ELIF_ELSE,
	T_STMT_FOR
} T_Statement_Type;

typedef enum
{
	T_EXPR_BINARY,
	T_EXPR_UNARY,
	T_EXPR_TRUE,
	T_EXPR_FALSE,
	T_EXPR_NEW,
	T_EXPR_NIL,
	T_EXPR_CALL,
	T_EXPR_CONST_INT,
	T_EXPR_CONST_CHAR,
	T_EXPR_ATOM
} T_Expr_Type;

template <class t_class>
class _List
{
private:
	vector<t_class *> _list;

public:
	_List() : _list() {}
	void append(t_class *obj)
	{
		_list.push_back(obj);
	}
	void print()
	{
		for (t_class *it : _list)
			it->print();
	}

	int length()
	{
		return _list.size();
	}

	t_class *at(int index)
	{
		return _list[index];
	}
};

class Id
{
private:
	string name;

public:
	Id(string _name)
	{
		name = _name;
	}

	string getName()
	{
		return name;
	}

	friend ostream &operator<<(ostream &os, const Id *id)
	{
		os << id->name;
		return os;
	}
};

class TonyType
{
private:
	T_Type_Type type_type;
	TonyType *type;

public:
	TonyType(T_Type_Type _type)
	{
		type_type = _type;
		if (type_type == T_TYPE_LIST)
			type = new TonyType(T_TYPE_NONE);
	}

	TonyType(TonyType *_type, T_Type_Type _type_type)
	{
		type_type = _type_type;
		type = _type;
	}

	int equals(TonyType *type)
	{
		TonyType *tmp1, *tmp2;
		tmp1 = this;
		tmp2 = type;
		//cout << "Comparing equality of: " << this << " and " << type << endl;
		while (tmp1->get_Type() == tmp2->get_Type())
		{
			if (tmp1->getType() != nullptr && tmp2->getType() != nullptr)
			{
				tmp1 = tmp1->getType();
				tmp2 = tmp2->getType();
			}
			else
			{
				if (tmp1->get_Type() != tmp2->get_Type())
				{
					cout << "TypeEqual: Given types are not equal: " << endl
							 << "\t\t" << this << endl
							 << "\t\t" << type << endl;
					return 1;
				}
				else
					return 0;
			}
		}
		cout << "Type Equality Error:" << endl
				 << "\tGiven types are not equal:" << endl
				 << "\t\t" << this << endl
				 << "\t\t" << type << endl;
		return 1;
	}

	T_Type_Type get_Type()
	{
		return type_type;
	}

	void setType(TonyType *_type)
	{
		type = _type;
	}

	TonyType *getType()
	{
		return type;
	}

	friend ostream &operator<<(ostream &os, const TonyType *type)
	{
		switch (type->type_type)
		{
		case T_TYPE_NONE:
			os << "void";
			break;
		case T_TYPE_INT:
			os << "int";
			break;
		case T_TYPE_BOOL:
			os << "bool";
			break;
		case T_TYPE_CHAR:
			os << "char";
			break;
		case T_TYPE_ARRAY:
			os << type->type << "[]";
			break;
		case T_TYPE_LIST:
			os << "list[" << type->type << "]";
			break;
		}
	}
};