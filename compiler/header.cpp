#include "header.hpp"

Header::Header(Id *_id, _List<Formal> *_formal_list, TonyType *t)
{
	id = _id;
	formal_list = _formal_list;
	type = t;
}

Header::~Header()
{
	delete formal_list;
	delete type;
}

TonyType *Header::getType()
{
	if (type != nullptr)
		return type;
	else
		cout << "Header getType nullptr" << endl;
}

void Header::sem()
{
	if (type == nullptr)
		type = new TonyType(T_TYPE_NONE);
	st.insert(id->getName(), formal_list, type);
	st.openScope();
	type_stack.push(type);
	if (formal_list != nullptr)
		for (int i = 0; i < formal_list->length(); i++)
			formal_list->at(i)->sem();
}

void Header::decl()
{
	if (type == nullptr)
		type = new TonyType(T_TYPE_NONE);
	st.insert(id->getName(), formal_list, type, 1);
}

Value* Header::codegen() {
	
}