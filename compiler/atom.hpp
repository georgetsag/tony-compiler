#pragma once
#include "semantics.hpp"

class Expression;
class Call;

using namespace std;

class Atom
{
private:
	T_Atom_Type atom_type;

	TonyType *type;

	Id *id;

	string str;

	Expression *expr;
	Atom *atom;

	Call *call;

public:
	void sem();

	Value* codegen();

	TonyType *getType();

	T_Atom_Type getAtomType();

	Atom(Id *_id);

	Atom(string _str);

	Atom(Atom *_atom, Expression *_expr);
	Atom(Call *_call);
	~Atom();

	friend ostream &operator<<(ostream &os, const Atom *atom)
	{
		switch (atom->atom_type)
		{
		case T_ATOM_ID:
			os << atom->id;
			break;
		case T_ATOM_CONST_STRING:
			os << "\"" << atom->str << "\"";
			break;
		case T_ATOM_ARRAY:
			os << atom->atom << "[]";
			break;
		case T_ATOM_CALL:
			os << atom->call;
			break;
		}
		return os;
	}
};
