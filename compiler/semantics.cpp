#include "semantics.hpp"

int formal_equals(Formal *formal, Formal *_formal)
{
	if (formal->getType()->equals(_formal->getType()) != 0)
	{
		cout << "Error: formal types not matching:" << endl;
		return 1;
	}
	if (formal->getIdList()->length() != _formal->getIdList()->length())
	{
		cout << "Error: formal parameters count not matching:" << endl;
		return 1;
	}
	if (formal->getRefType() != _formal->getRefType())
	{
		cout << "Error: one formal is declared as 'ref', while the other isn't" << endl;
		return 1;
	}
	for (int i = 0; i < formal->getIdList()->length(); i++)
	{
		if (formal->getIdList()->at(i)->getName().compare(_formal->getIdList()->at(i)->getName()) != 0)
		{
			cout << "Error: formals not compatible:" << endl;
			cout << "\tFunction argument identifiers must be the same: " << formal->getIdList()->at(i)->getName() << " " << _formal->getIdList()->at(i)->getName() << endl;
			return 1;
		}
	}
	return 0;
}

Call::Call(Id *_id, _List<Expression> *_arg_list)
{
	id = _id;
	arg_list = _arg_list;
}

Call::Call(Id *_id)
{
	id = _id;
}

void Call::sem()
{
	SymbolEntry *se = st.lookup(id->getName());
	type = se->type;

	if (se->symbol_type != T_SYMBOL_FUNC)
	{
		cout << "Error: " << id->getName() << " is not a function" << endl;
		exit(1);
	}
	if (arg_list != nullptr)
		for (int i = 0; i < arg_list->length(); i++)
			arg_list->at(i)->sem();
	if (arg_list != nullptr && se->func_args != nullptr)
		if (Call::matchCallArguments(arg_list, se->func_args) != 0)
		{
			cout << "Error: Failed to match arguments for function call: " << this << endl;
			exit(1);
		}
	if (arg_list != nullptr && se->func_args == nullptr)
	{
		cout << "Error: Failed to match arguments for function call: " << this << endl;
		exit(1);
	}
	if (arg_list == nullptr && se->func_args != nullptr)
	{
		cout << "Error: Failed to match arguments for function call: " << this << endl;
		exit(1);
	}
}

Value* Call::codegen() {

}

int Call::matchCallArguments(_List<Expression> *arg_list, _List<Formal> *formal_list)
{
	int i_arg = 0;
	for (int i = 0; i < formal_list->length(); i++)
	{
		_List<Id> *id_list;
		id_list = formal_list->at(i)->getIdList();
		for (int j = 0; j < id_list->length(); j++)
		{
			if (i_arg == arg_list->length())
				return 1;
			if (arg_list->at(i_arg)->getType()->equals(formal_list->at(i)->getType()) != 0)
			{
				cout << "Non matching argument: " << arg_list->at(i_arg) << ", expected: " << formal_list->at(i)->getType() << endl;
				return 1;
			}
			if (formal_list->at(i)->getRefType() == 1)
				if (arg_list->at(i_arg)->getExprType() != T_EXPR_ATOM)
				{
					cout << "Error: Argument passed is not L-value: " << arg_list->at(i_arg) << endl;
					return 1;
				}
				else if (arg_list->at(i_arg)->getAtom()->getAtomType() != T_ATOM_ARRAY && arg_list->at(i_arg)->getAtom()->getAtomType() != T_ATOM_ID)
				{
					cout << "Error: Argument passed is not L-value: " << arg_list->at(i_arg) << endl;
					return 1;
				}
			i_arg++;
		}
	}
	if (i_arg != arg_list->length())
		return 1;
	return 0;
}

TonyType *Call::getType()
{
	return type;
}

void Simple::sem()
{
	switch (simple_type)
	{
	case T_SIMPLE_SKIP:
		//TODO?
		break;
	case T_SIMPLE_ASSIGN:
		atom->sem();
		expr->sem();
		if (atom->getAtomType() != T_ATOM_ID && atom->getAtomType() != T_ATOM_ARRAY)
		{
			cout << "Non L-value in assignment: " << this << endl;
			exit(1);
		}
		if (atom->getType()->equals(expr->getType()) != 0)
		{
			cout << "Incompatible types in assignment: " << this << endl;
			exit(1);
		}
		break;
	case T_SIMPLE_CALL:
		call->sem();
		break;
	}
}

Value* Simple::codegen() {

}

Simple::Simple(T_Simple_Type _type)
{
	simple_type = _type;
}

Simple::Simple(Atom *_atom, Expression *_expr)
{
	atom = _atom;
	expr = _expr;
	simple_type = T_SIMPLE_ASSIGN;
}

Simple::Simple(Call *_call)
{
	call = _call;
	simple_type = T_SIMPLE_CALL;
}

void Statement::sem()
{
	switch (stmt_type)
	{
	case T_STMT_SIMPLE:
		simple->sem();
		break;
	case T_STMT_EXIT:
		TonyType *tmp;
		tmp = new TonyType(T_TYPE_NONE);
		if (tmp->equals(type_stack.top()) != 0)
		{
			cout << "Error: 'exit' inside a function returning non-void value" << endl;
			exit(1);
		}
		break;
	case T_STMT_RETURN:
		expr->sem();
		if (expr->getType()->equals(type_stack.top()) != 0)
		{
			cout << "Error: return statement doesn't match function return type:\n\t return " << expr << endl;
			exit(1);
		}
		break;
	case T_STMT_IF:
		expr->sem();
		if (expr->getType()->get_Type() != T_TYPE_BOOL)
			error("Non Bool expression in IF statement");
		for (int i = 0; i < stmt_list_1->length(); i++)
			stmt_list_1->at(i)->sem();
		break;
	case T_STMT_IF_ELIF:
		expr->sem();
		if (expr->getType()->get_Type() != T_TYPE_BOOL)
			error("Non Bool expression in IF statement");
		for (int i = 0; i < stmt_list_1->length(); i++)
			stmt_list_1->at(i)->sem();
		for (int i = 0; i < else_if_list->length(); i++)
			else_if_list->at(i)->sem();
		break;
	case T_STMT_IF_ELSE:
		expr->sem();
		if (expr->getType()->get_Type() != T_TYPE_BOOL)
			error("Non Bool expression in IF statement");
		for (int i = 0; i < stmt_list_1->length(); i++)
			stmt_list_1->at(i)->sem();
		for (int i = 0; i < stmt_list_2->length(); i++)
			stmt_list_2->at(i)->sem();

		break;
	case T_STMT_IF_ELIF_ELSE:
		expr->sem();
		if (expr->getType()->get_Type() != T_TYPE_BOOL)
			error("Non Bool expression in IF statement");
		for (int i = 0; i < stmt_list_1->length(); i++)
			stmt_list_1->at(i)->sem();
		for (int i = 0; i < else_if_list->length(); i++)
			else_if_list->at(i)->sem();
		for (int i = 0; i < stmt_list_2->length(); i++)
			stmt_list_2->at(i)->sem();
		break;
	case T_STMT_FOR:
		for (int i = 0; i < simple_list_1->length(); i++)
			simple_list_1->at(i)->sem();
		for (int i = 0; i < simple_list_2->length(); i++)
			simple_list_2->at(i)->sem();
		expr->sem();
		if (expr->getType()->get_Type() != T_TYPE_BOOL)
			error("Non Bool expression in FOR statement");
		for (int i = 0; i < stmt_list_1->length(); i++)
			stmt_list_1->at(i)->sem();
		break;
	}
}

Value* Statement::codegen() {

}

Statement::Statement(Simple *_simple)
{
	simple = _simple;
	stmt_type = T_STMT_SIMPLE;
}

Statement::Statement(T_Statement_Type _stmt_type)
{
	stmt_type = _stmt_type;
}

Statement::Statement(Expression *_expr, T_Statement_Type _stmt_type)
{
	expr = _expr;
	stmt_type = _stmt_type;
}

Statement::Statement(Expression *_expr, _List<Statement> *_stmt_list)
{
	expr = _expr;
	stmt_list_1 = _stmt_list;
	stmt_type = T_STMT_IF;
}

Statement::Statement(Expression *_expr, _List<Statement> *_stmt_list, _List<Else_If> *_else_if_list)
{
	expr = _expr;
	stmt_list_1 = _stmt_list;
	else_if_list = _else_if_list;
	stmt_type = T_STMT_IF_ELIF;
}

Statement::Statement(Expression *_expr, _List<Statement> *_stmt_list_1, _List<Statement> *_stmt_list_2)
{
	expr = _expr;
	stmt_list_1 = _stmt_list_1;
	stmt_list_2 = _stmt_list_2;
	stmt_type = T_STMT_IF_ELSE;
}

Statement::Statement(Expression *_expr, _List<Statement> *_stmt_list_1, _List<Statement> *_stmt_list_2, _List<Else_If> *_else_if_list)
{
	expr = _expr;
	stmt_list_1 = _stmt_list_1;
	stmt_list_2 = _stmt_list_2;
	else_if_list = _else_if_list;
	stmt_type = T_STMT_IF_ELIF_ELSE;
}

Statement::Statement(Expression *_expr, _List<Simple> *_simple_list_1, _List<Simple> *_simple_list_2, _List<Statement> *_stmt_list)
{
	expr = _expr;
	simple_list_1 = _simple_list_1;
	simple_list_2 = _simple_list_2;
	stmt_list_1 = _stmt_list;
	stmt_type = T_STMT_FOR;
}

void Definition::sem()
{
	switch (def_type)
	{
	case T_FUNC_DEF:
		header->sem();
		if (def_list != nullptr)
			for (int i = 0; i < def_list->length(); i++)
			{
				def_list->at(i)->sem();
			}
		for (int i = 0; i < stmt_list->length(); i++)
		{
			stmt_list->at(i)->sem();
		}
		type_stack.pop();
		st.closeScope();
		break;
	case T_FUNC_DECL:
		header->decl();
		break;
	case T_VAR_DEF:
		for (int i = 0; i < id_list->length(); i++)
		{
			st.insert(id_list->at(i)->getName(), type);
			//cout << "Defined " << type << " : " << id_list->at(i)->getName() << endl;
		}

		break;
	}
}

Value* Definition::codegen() {
	
}

T_Def_Type Definition::get_def_type()
{
	return def_type;
}

void Definition::init()
{
	try
	{
		st.openScope();

		_List<Formal> *fl_puti;
		fl_puti = new _List<Formal>();
		_List<Id> *idl_puti;
		idl_puti = new _List<Id>();
		idl_puti->append(new Id("n"));
		fl_puti->append(new Formal(
				0, new TonyType(T_TYPE_INT), idl_puti));
		st.insert("puti", fl_puti, new TonyType(T_TYPE_NONE));

		_List<Formal> *fl_putb;
		fl_putb = new _List<Formal>();
		_List<Id> *idl_putb;
		idl_putb = new _List<Id>();
		idl_putb->append(new Id("b"));
		fl_putb->append(new Formal(
				0, new TonyType(T_TYPE_BOOL), idl_putb));
		st.insert("putb", fl_putb, new TonyType(T_TYPE_NONE));

		_List<Formal> *fl_putc;
		fl_putc = new _List<Formal>();
		_List<Id> *idl_putc;
		idl_putc = new _List<Id>();
		idl_putc->append(new Id("c"));
		fl_putc->append(new Formal(
				0, new TonyType(T_TYPE_CHAR), idl_putc));
		st.insert("putc", fl_putc, new TonyType(T_TYPE_NONE));

		_List<Formal> *fl_puts;
		fl_puts = new _List<Formal>();
		_List<Id> *idl_puts;
		idl_puts = new _List<Id>();
		idl_puts->append(new Id("s"));
		fl_puts->append(new Formal(
				0, new TonyType(new TonyType(T_TYPE_CHAR), T_TYPE_ARRAY), idl_puts));
		st.insert("puts", fl_puts, new TonyType(T_TYPE_NONE));

		st.insert("geti", nullptr, new TonyType(T_TYPE_INT));
		st.insert("getb", nullptr, new TonyType(T_TYPE_BOOL));
		st.insert("getc", nullptr, new TonyType(T_TYPE_CHAR));

		_List<Formal> *fl_gets;
		fl_gets = new _List<Formal>();
		_List<Id> *idl_gets1;
		idl_gets1 = new _List<Id>();
		idl_gets1->append(new Id("n"));
		fl_gets->append(new Formal(
				0, new TonyType(T_TYPE_INT), idl_gets1));
		_List<Id> *idl_gets2;
		idl_gets2 = new _List<Id>();
		idl_gets2->append(new Id("s"));
		fl_gets->append(new Formal(
				0, new TonyType(new TonyType(T_TYPE_CHAR), T_TYPE_ARRAY), idl_gets2));
		st.insert("gets", fl_gets, new TonyType(T_TYPE_NONE));

		_List<Formal> *fl_abs;
		fl_abs = new _List<Formal>();
		_List<Id> *idl_abs;
		idl_abs = new _List<Id>();
		idl_abs->append(new Id("n"));
		fl_abs->append(new Formal(
				0, new TonyType(T_TYPE_INT), idl_abs));
		st.insert("abs", fl_abs, new TonyType(T_TYPE_INT));

		_List<Formal> *fl_ord;
		fl_ord = new _List<Formal>();
		_List<Id> *idl_ord;
		idl_ord = new _List<Id>();
		idl_ord->append(new Id("c"));
		fl_ord->append(new Formal(
				0, new TonyType(T_TYPE_CHAR), idl_ord));
		st.insert("ord", fl_ord, new TonyType(T_TYPE_INT));

		_List<Formal> *fl_chr;
		fl_chr = new _List<Formal>();
		_List<Id> *idl_chr;
		idl_chr = new _List<Id>();
		idl_chr->append(new Id("n"));
		fl_chr->append(new Formal(
				0, new TonyType(T_TYPE_INT), idl_chr));
		st.insert("chr", fl_chr, new TonyType(T_TYPE_CHAR));

		_List<Formal> *fl_strlen;
		fl_strlen = new _List<Formal>();
		_List<Id> *idl_strlen;
		idl_strlen = new _List<Id>();
		idl_strlen->append(new Id("s"));
		fl_strlen->append(new Formal(
				0, new TonyType(new TonyType(T_TYPE_CHAR), T_TYPE_ARRAY), idl_strlen));
		st.insert("strlen", fl_strlen, new TonyType(T_TYPE_INT));

		_List<Formal> *fl_strcmp;
		fl_strcmp = new _List<Formal>();
		_List<Id> *idl_strcmp;
		idl_strcmp = new _List<Id>();
		idl_strcmp->append(new Id("s1"));
		idl_strcmp->append(new Id("s2"));
		fl_strcmp->append(new Formal(
				0, new TonyType(new TonyType(T_TYPE_CHAR), T_TYPE_ARRAY), idl_strcmp));
		st.insert("strcmp", fl_strcmp, new TonyType(T_TYPE_INT));

		_List<Formal> *fl_strcpy;
		fl_strcpy = new _List<Formal>();
		_List<Id> *idl_strcpy;
		idl_strcpy = new _List<Id>();
		idl_strcpy->append(new Id("trg"));
		idl_strcpy->append(new Id("src"));
		fl_strcpy->append(new Formal(
				0, new TonyType(new TonyType(T_TYPE_CHAR), T_TYPE_ARRAY), idl_strcpy));
		st.insert("strcpy", fl_strcpy, new TonyType(T_TYPE_NONE));

		_List<Formal> *fl_strcat;
		fl_strcat = new _List<Formal>();
		_List<Id> *idl_strcat;
		idl_strcat = new _List<Id>();
		idl_strcat->append(new Id("trg"));
		idl_strcat->append(new Id("src"));
		fl_strcat->append(new Formal(
				0, new TonyType(new TonyType(T_TYPE_CHAR), T_TYPE_ARRAY), idl_strcat));
		st.insert("strcat", fl_strcat, new TonyType(T_TYPE_NONE));
	}
	catch (...)
	{
		cout << "Failed to init Definitions" << endl;
		exit(1);
	}
}

Definition::Definition(TonyType *t, _List<Id> *list)
{
	type = t;
	id_list = list;
	def_type = T_VAR_DEF;
}

Definition::Definition(Header *_header)
{
	header = _header;
	def_type = T_FUNC_DECL;
}

Definition::Definition(Header *_header, _List<Definition> *_def_list, _List<Statement> *_stmt_list)
{
	header = _header;
	def_list = _def_list;
	stmt_list = _stmt_list;
	def_type = T_FUNC_DEF;
}

T_Def_Type Definition::getDefType()
{
	return def_type;
}
