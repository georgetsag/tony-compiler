/**DEFINITIONS**/

%{
#include <cstdio>
#include <iostream>
#include <string>
#include <string.h>
#include <execinfo.h>
#include <stack>
#include <signal.h>
#include "lexer.hpp"
#include "error.h"
#include "semantics.hpp"
#include "symbol.hpp"

using namespace std;
extern int yylex();
extern int yyparse();
extern FILE *yyin;
const char * filename = "file.in";
void yyerror(const char *s);
extern int linecount;

SymbolTable st;
stack<TonyType *> type_stack;

%}

%union {
	_List<Formal> *formal_list;
	_List<Definition> *definition_list;
	_List<Expression> *expr_list;
	_List<Simple> *simple_list;
	_List<Else_If> *else_if_list;
	_List<Statement> *stmt_list;
	_List<Id> *id_list;

	Formal *formal;
	Definition *definition;
	Header *header;
	TonyType *type;
	Call *call;
	Expression *expr;
	Atom *atom;
	Simple *simple;
	Else_If *else_if;
	Statement *stmt;
	Id *id;

	int ival;
	char cval;
	char* sval;
}

/**TOKENS**/
%token T_eof
%token T_and
%token T_bool
%token T_char
%token T_decl
%token T_def
%token T_else
%token T_elif
%token T_end
%token T_exit
%token T_false
%token T_for
%token T_head
%token T_if
%token T_int
%token T_list
%token T_mod
%token T_new
%token T_nil
%token T_nil_
%token T_not
%token T_or
%token T_ref
%token T_return
%token T_skip
%token T_tail
%token T_true

%token T_assign
%token T_not_equal
%token T_less_equal
%token T_greater_equal


%token <ival>	T_const_int
%token <sval>	T_const_char
%token <sval>	T_const_string
%token <sval> 	T_id

%type <ival>		program
%type <id_list>		id_l
%type <definition>	general_def
%type <definition_list>	general_def_l
%type <definition>	func_def
%type <header>		header
%type <formal>		formal
%type <formal_list> 	formal_l
%type <type>		type
%type <definition>	func_decl
%type <definition>	var_def
%type <stmt>		stmt
%type <stmt_list>	stmt_l
%type <else_if_list>	elif_l
%type <simple>		simple
%type <simple_list>	simple_l
%type <call>		call
%type <atom>		atom
%type <expr>		expr
%type <expr_list>	expr_l


%left T_or
%left T_and
%right T_not
%nonassoc '=' T_not_equal '<' '>' T_less_equal T_greater_equal
%right '#'
%left '+' '-'
%left '*' '/' T_mod
%left UMINUS
%left UPLUS
/**DEFINITIONS END**/


%%

program:
	func_def T_eof 
	{
	$1->init();
	$1->sem();
	TonyBuilder::build();
	return 0;
	}
	;

func_def:
	T_def header ':' general_def_l stmt_l T_end
	{
	$$ = new Definition($2, $4, $5);
	}
	|
	T_def header ':' stmt_l T_end
	{
	$$ = new Definition($2, nullptr, $4);
	}
	;

general_def:
	func_def {$$ = $1;}
	|
	func_decl {$$ = $1;}
	|
	var_def {$$ = $1;}
	;

general_def_l:
	general_def_l general_def
	{
	$1->append($2);
	$$ = $1;
	}
	|
	general_def
	{
	$$ = new _List<Definition>();
	$$->append($1);
	}
	;

header:
	T_id '(' formal_l ')'
	{
	Id *id = new Id($1);
	$$ = new Header(id, $3, nullptr);
	}
	|
	type T_id '(' formal_l ')'
	{
	Id *id = new Id($2);
	$$ = new Header(id, $4, $1);
	}
	|
	T_id '(' ')'
	{
	Id *id = new Id($1);
	$$ = new Header(id, nullptr, nullptr);
	}
	|
	type T_id '(' ')'
	{
	Id *id = new Id($2);
	$$ = new Header(id, nullptr, $1);
	}
	;

formal:
	T_ref type id_l {$$ = new Formal(1, $2, $3);}
	|
	type id_l {$$ = new Formal(0, $1, $2);}
	;

formal_l:
	
	formal_l ';' formal
	{
	$1->append($3);
	$$ = $1;
	}
	|
	formal
	{
	$$ = new _List<Formal>();
	$$->append($1);
	}
	;

func_decl:
	T_decl header
	{$$ = new Definition($2);}
	;

var_def:
	type id_l
	{$$ = new Definition($1, $2);}
	;

id_l:
	id_l ',' T_id
	{
	Id *id = new Id($3);
	$1->append(id);
	$$ = $1;
	}
	|
	T_id
	{
	Id *id = new Id($1);
	$$ = new _List<Id>();
	$$->append(id);
	}
	;

stmt:
	simple
	{$$ = new Statement($1);}
	|
	T_exit
	{$$ = new Statement(T_STMT_EXIT);}
	|
	T_return expr
	{$$ = new Statement($2, T_STMT_RETURN);}
	|
	T_if expr ':' stmt_l T_end
	{$$ = new Statement($2, $4);}
	|
	T_if expr ':' stmt_l elif_l T_end
	{$$ = new Statement($2, $4, $5);}
	|
	T_if expr ':' stmt_l T_else ':' stmt_l T_end
	{$$ = new Statement($2, $4, $7);}
	|
	T_if expr ':' stmt_l elif_l T_else ':' stmt_l T_end
	{$$ = new Statement($2, $4, $8, $5);}
	|
	T_for simple_l ';' expr ';' simple_l ':' stmt_l T_end
	{$$ = new Statement($4, $2, $6, $8);}
	;


elif_l:
	elif_l T_elif expr ':' stmt_l
	{
	Else_If *elif = new Else_If($3, $5);
	$1->append(elif);
	$$ = $1;
	}
	|
	T_elif expr ':' stmt_l
	{
	Else_If *elif = new Else_If($2, $4);
	$$ = new _List<Else_If>();
	$$-> append(elif);
	}
	;
	
stmt_l:
	stmt_l stmt		{$1->append($2); $$ = $1;}
	|
	stmt			{$$ = new _List<Statement>(); $$->append($1);}
	;

type:
	T_int			{$$ = new TonyType(T_TYPE_INT);}
	|
	T_bool			{$$ = new TonyType(T_TYPE_BOOL);}
	|
	T_char			{$$ = new TonyType(T_TYPE_CHAR);}
	|
	type '[' ']'		{$$ = new TonyType($1, T_TYPE_ARRAY);}
	|
	T_list '[' type ']'	{$$ = new TonyType($3, T_TYPE_LIST);}
	;

simple:
	T_skip			{$$ = new Simple(T_SIMPLE_SKIP);}
	|
	atom T_assign expr	{$$ = new Simple($1, $3);}
	|
	call			{$$ = new Simple($1);}
	;

simple_l:
	simple_l ',' simple	{$1->append($3); $$ = $1;}
	|
	simple			{$$ = new _List<Simple>(); $$->append($1);}
	;

call:
	T_id '(' expr_l ')'	{ Id *id = new Id($1); $$ = new Call(id, $3); }
	|
	T_id '(' ')'		{ Id *id = new Id($1); $$ = new Call(id);}
	;


atom:
	T_id			{Id *id = new Id($1); $$ = new Atom(id);}
	|
	T_const_string		{$$ = new Atom($1);}
	|
	atom '[' expr ']'	{$$ = new Atom($1, $3);}
	|
	call			{$$ = new Atom($1);}
	;

expr:
	atom			{$$ = new Expression($1);}
	|
	T_const_int		{$$ = new Expression($1);}
	|
	T_const_char		{$$ = new Expression($1);}
	|
	'(' expr ')'		{$$ = new Expression($2, "()");}
	|
	'+' expr %prec UPLUS		{$$ = new Expression($2, "+");}
	|
	'-' expr %prec UMINUS		{$$ = new Expression($2, "-");}
	|
	expr '+' expr		{$$ = new Expression($1, $3, "+");}
	|
	expr '-' expr		{$$ = new Expression($1, $3, "-");}
	|
	expr '*' expr		{$$ = new Expression($1, $3, "*");}
	|
	expr T_mod expr		{$$ = new Expression($1, $3, "mod");}
	|
	expr '=' expr		{$$ = new Expression($1, $3, "=");}
	|
	expr T_not_equal expr	{$$ = new Expression($1, $3, "!=");}
	|
	expr '<' expr		{$$ = new Expression($1, $3, "<");}
	|
	expr '>' expr		{$$ = new Expression($1, $3, ">");}
	|
	expr T_less_equal expr	{$$ = new Expression($1, $3, "<=");}
	|
	expr T_greater_equal expr	{$$ = new Expression($1, $3, ">=");}
	|
	T_true			{$$ = new Expression(T_EXPR_TRUE);}
	|
	T_false			{$$ = new Expression(T_EXPR_FALSE);}
	|
	T_not expr		{$$ = new Expression($2, "not");}
	|
	expr T_and expr		{$$ = new Expression($1, $3, "and");}
	|
	expr T_or expr		{$$ = new Expression($1, $3, "or");}
	|
	T_new type '[' expr ']'	{$$ = new Expression($4, $2);}
	|
	T_nil			{$$ = new Expression(T_EXPR_NIL);}
	|
	T_nil_ '(' expr ')'	{$$ = new Expression($3, "nil?");}
	|
	expr '#' expr		{$$ = new Expression($1, $3, "#");}
	|
	T_head '(' expr ')' 	{$$ = new Expression($3, "head");}
	|
	T_tail '(' expr ')' 	{$$ = new Expression($3, "tail");}

	;


expr_l:
	expr 	
	{ 
	$$ = new _List<Expression>();
	$$->append($1);
	}
	|
	expr_l ',' expr 
	{
	$1->append($3);
	$$ = $1;
	}
	;


%%

void handler(int sig) {
  void *array[10];
  size_t size;

  // get void*'s for all entries on the stack
  size = backtrace(array, 10);

  // print out all the frames to stderr
  fprintf(stderr, "Error: signal %d:\n", sig);
  backtrace_symbols_fd(array, size, fileno(stderr));
  exit(1);
}

void yyerror(const char *s){
	cout << "ERROR:(" << linecount << ") " << s << endl;
}

int main(int argc, char **argv) {
	//yydebug = 1;
	signal(SIGSEGV, handler); 
	if(argc!=2){
		cout << "Wrong args" << endl;
		exit(1);
	}
	FILE *inFile = fopen(argv[1], "r");
	if(!inFile){
		cout << "Could not open file" << endl;
		return -1;
	}
	yyin = inFile;
	yyparse();
	return 0;
}
