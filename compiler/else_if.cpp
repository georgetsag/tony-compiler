#include "else_if.hpp"

using namespace std;

Else_If::Else_If(Expression *_expr, _List<Statement> *_stmt_list)
{
	expr = _expr;
	stmt_list = _stmt_list;
}

void Else_If::sem()
{
	expr->sem();
	for (int i = 0; i < stmt_list->length(); i++)
		stmt_list->at(i)->sem();
}

Value* Else_If::codegen() {
	
}
