#pragma once
#include "semantics.hpp"

using namespace std;

class Formal
{
private:
	int ref_val;
	TonyType *type;
	_List<Id> *id_list;

public:
	void sem();
	Value* codegen();

	TonyType *getType();
	_List<Id> *getIdList();
	int getRefType();
	int equals(Formal *_formal);

	Formal(int _ref_val, TonyType *_type, _List<Id> *_id_list);

	friend ostream &operator<<(ostream &os, const Formal *formal)
	{
		if (formal->ref_val)
			os << "ref ";
		os << formal->type << " ";
		for (int i = 0; i < formal->id_list->length(); i++)
		{
			os << formal->id_list->at(i) << ",";
		}
	}
};
