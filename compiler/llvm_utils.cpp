#include "llvm_utils.hpp"


LLVMContext TonyBuilder::TheContext;
IRBuilder<> TonyBuilder::Builder(TheContext);
std::unique_ptr<Module> TonyBuilder::TheModule;
std::unique_ptr<legacy::FunctionPassManager> TonyBuilder::TheFPM;
std::map<std::string, Value *> TonyBuilder::NamedValues;

GlobalVariable *TonyBuilder::TheVars;
GlobalVariable *TonyBuilder::TheNL;
Function *TonyBuilder::TheWriteInteger;
Function *TonyBuilder::TheWriteString;
Function *TonyBuilder::TheWriteBoolean;
Function *TonyBuilder::TheWriteCharacter;
Function *TonyBuilder::TheInit;
Function *TonyBuilder::TheMalloc;

Type *TonyBuilder::i8;
Type *TonyBuilder::i32;
Type *TonyBuilder::i64;
Type *TonyBuilder::TheNodeTypePtr;
