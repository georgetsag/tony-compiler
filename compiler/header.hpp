#pragma once
#include "semantics.hpp"

class Formal;
class TonyType;

class Header
{
private:
	Id *id;
	_List<Formal> *formal_list;
	TonyType *type;

public:
	Header(Id *_id, _List<Formal> *_formal_list, TonyType *t);
	~Header();

	friend ostream &operator<<(ostream &os, const Header *hd)
	{
		os << hd->id << "(" << hd->formal_list->length() << " args)";
		return os;
	}

	TonyType *getType();

	void sem();
	void decl();
	Value* codegen();
};
